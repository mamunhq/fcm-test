<?php
/*
 * Simple PHP script showing how to send an Android push notification.
 * Be sure to replace the API_ACCESS_KEY with a proper one from the Google API's Console page.
 * To use the script, just call scriptName.php?id=THE_DEVICE_REGISTRATION_ID
 */
// API access key from Google API's Console
define( 'API_ACCESS_KEY', 'YOUR-API-ACCESS-KEY-GOES-HERE' );

//you need to add device registration id. if you want to send multiple device same time make an array with id and add here.
$registrationIds = array( $_GET['id'] );

// prep the bundle
$msg = array
(
    'full_data' 	=> array(
        'news_title' => 'Breaking News',
        'news_details' => 'Bangladesh won by 50 runs against England',
        'news_id' => '',  // when type is news need to put news id otherwise blank
        'type' => 'breaking' // type value will be breaking or news
    )
);

$fields = array
(
    'registration_ids' 	=> $registrationIds,
    'data'			=> $msg
);

$headers = array
(
    'Authorization: key=' . API_ACCESS_KEY,
    'Content-Type: application/json'
);

$ch = curl_init();
curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
curl_setopt( $ch,CURLOPT_POST, true );
curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
$result = curl_exec($ch );
curl_close( $ch );

echo $result;
